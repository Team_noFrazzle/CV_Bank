<?php
namespace App\Resumer\Posts;

use pdo;


class Posts
{
    public $id = '';
    public $userid = '';
    public $title = '';
    public $error = '';
    public $description = '';
    public $tag = '';
    public $categories = '';
    public $uniqueid='';

    public function setData($data = '', $userid = '',$uniqueid='')
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('tag', $data)) {
            $this->tag = $data['tag'];
        }
        if (array_key_exists('categories', $data)) {
            $this->categories = $data['categories'];
        }
        if ($userid) {
            $this->userid = $userid;
        }if ($uniqueid) {
            $this->uniqueid = $uniqueid;
        }

        return $this;


    }

    public function __construct()
    {

        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }

    public function Store()
    {
        try {
            $query = "INSERT INTO `posts` (`id`, `user_id`, `title`, `description`, `tags`, `categories`) VALUES ( :a, :b, :p,:d,:e,:f )";


            $stmt = $this->pdo->prepare($query);

            $data = $stmt->execute(array(
                ':a' => null,
                ':b' => $this->userid,
                ':p' => $this->title,
                ':d' => $this->description,
                ':e' => $this->tag,
                ':f' => $this->categories
            ));

            if ($data) {
                $_SESSION['message'] = 'Successfully ! Submitted';
                header("location:Create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function index()
    {

        try {

            $query = "SELECT *FROM posts WHERE  id= $this->uniqueid";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            $data = $stmt->fetch();


            return $_SESSION['data'] = $data;


        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $query = "UPDATE `posts` SET `title` = :p, `description` = :d, `tags` = :e, `categories` =:f WHERE `posts`.`id` =$this->id";

            $stmt = $this->pdo->prepare($query);


//            print_r($query);
//            die();
            $data = $stmt->execute(array(
                ':p' => $this->title,
                ':d' => $this->description,
                ':e' => $this->tag,
                ':f' => $this->categories
            ));

            if ($data) {
                $_SESSION['message'] = 'Successfully ! Updated';
                header("location:EditForm.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function view()
    {
        try {
            // $query  = "SELECT p.* ,c.* FROM experiences as p , users as c WHERE c.id=p.user_id";
            $query =
                "SELECT posts.id, posts.title, posts.description, posts.tags, posts.categories FROM  posts   LEFT JOIN users 
                ON posts.user_id = users.id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            $data = $stmt->fetchAll();

            return $_SESSION['data'] = $data;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Delete()
    {


        try {
            $query = "Delete From posts WHERE  id=$this->uniqueid";


            $stmt = $this->pdo->prepare($query);
            $result = $stmt->execute();

            if ($result) {
                $_SESSION['message'] = 'Successfully Deleted';
                header("location:index.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function PassingYear()
    {


        try {
            $query = "Select *From year";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            return $result = $stmt->fetchAll();

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}
