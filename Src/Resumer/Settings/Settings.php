<?php
namespace App\Resumer\Settings;

use pdo;


class Settings
{


    public $id = '';
    public  $description ='';
    public $title='';
    public $error = '';
    public $img='';
    public $tmp='';
    public $allowedExtention='';
    public  $extention='';
    public $check='';
    public $address='';
    public $fullName='';
    public $userid='';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }
    public function setData($data = '',$userid='')
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
            $this->title   = filter_var($this->title, FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];

            $this->description = filter_var($this->description, FILTER_SANITIZE_STRING);
        }
        if ($userid) {
           $this->userid = $userid;

        }
        if (array_key_exists('fname', $data)) {
            $this->fullName = $data['fname'];
            $this->fullName = filter_var($this->fullName, FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('address', $data)) {
            $this->address = $data['address'];

            $this->address = filter_var($this->address, FILTER_SANITIZE_STRING);
        }
        if($_FILES) {
            $this->img = $_FILES['img']['name'];
            $this->tmp = $_FILES['img']['tmp_name'];
            $this->allowedExtention = array("jpg", "jpeg", "gif", "png");
            $this->extention = end(explode(".", $this->img));

            $this->check = ($this->extention == "jpg" || $this->extention == "png" || $this->extention == "gif" || $this->extention == "jpeg");
        }
        return $this;
    }
    public function Store()
    {
        try {
            move_uploaded_file($this->tmp,'../../../../image/'.$this->img );

            $query  = "INSERT INTO `settings` (`id`, `user_id`, `title`, `fullname`, `description`, `address`, `featured_img`) 
VALUES (:id, :user_id, :title, :fullname, :description, :address, :img)";
            $stmt =$this->pdo->prepare($query);
            $data = $stmt->execute(array(
              ':id'=>null,
                ':user_id'=>$this->userid,
                ':title'=>$this->title,
                ':fullname'=>$this->fullName,
                ':description'=>$this->description,
                ':address'=>$this->address,
                ':img'=>$this->img
            ));
            if ($data) {
                $_SESSION['message']='Successfully ! Submitted';
                header("location:Create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function index()
    {

        try {

            $query  = "SELECT *FROM settings WHERE  user_id=$this->userid";

            $stmt =$this->pdo->prepare($query);
             $stmt->execute();

          $data =$stmt->fetch();

        return  $_SESSION['data']=$data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function update()
    {


        try {

            move_uploaded_file($this->tmp,'../../../../image/'.$this->img );
            $query  = "UPDATE `settings` SET  `title` =:title, `fullname` =:fullname, `description` =:description, `address` =:address, `featured_img` =:img WHERE `settings`.`user_id` = $this->userid";

            $stmt = $this->pdo->prepare($query);
            $data =   $stmt->execute(array(

                ':title'=>$this->title,
                ':fullname'=>$this->fullName,
                ':description'=>$this->description,
                ':address'=>$this->address,
                ':img'=>$this->img
            ));

            if ($data) {
                $_SESSION['message']='Successfully ! Updated';
                header("location:EditForm.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function view()
    {


        try {

            $query  = "SELECT p.* ,c.* FROM settings as p , users as c WHERE p.user_id=c.id";

            $stmt =$this->pdo->prepare($query);
            $stmt->execute();

            $data =$stmt->fetchAll();

            return  $_SESSION['data']=$data;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Delete()
    {


        try {
            $query  = "Delete From Settings WHERE  user_id=$this->userid";


            $stmt =$this->pdo->prepare($query);
            $result=  $stmt->execute();

            if($result)
            {
                $_SESSION['message'] ='Successfully Deleted';
                header("location:index.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}
