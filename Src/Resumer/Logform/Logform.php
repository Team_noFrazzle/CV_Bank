<?php
namespace App\Resumer\Logform;
use PDO;
class Logform
{
    public $id,$username,$password,$email,$pdo,$key;
    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks','root','');
    }
    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('username',$data)){
            $this->username=$data['username'];
        }
        if(array_key_exists('password',$data)){
            $this->password=$data['password'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('key',$data)){
            $this->email=$data['key'];
        }
        return $this;
    }
    public function store()
    {
        try {
            $query = "INSERT INTO `users` (`id`,`unique_id`, `username`, `email`, `password`, `token`) VALUES (:id,:uid, :uname, :email, :pw, :tk)";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => null,
                    ':uid' => uniqid(),
                    ':uname' => $this->username,
                    ':email' => $this->email,
                    ':pw' => $this->password,
                    ':tk' => uniqid($this->username),

                )
            );
            if ($stmt) {
                $_SESSION['msg'] = "Successfully Registered<br>Please Verify Your Email Address";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
        public function userAvailability()
    {
        try {
            $query = "SELECT  * FROM `users` WHERE username = '$this->username' OR email = '$this->email'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function login()
    {
        try {
            $query = "SELECT  * FROM `users` WHERE ( username = '$this->username' OR email = '$this->username') AND password = '$this->password'";

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (!empty($data)) {
                $_SESSION['user_info'] = $data;
                $_SESSION['id']=$data['id'];
                header('location:../Admin/index.php');
            } else {
                $_SESSION['msg'] = "Invalid Username, Email Or Password";
                header('location:index.php');
            }

            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

}