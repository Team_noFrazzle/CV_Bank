<?php
namespace App\Resumer\Search;
use PDO;
class Search{
    public $check,$key;
    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks','root','');
    }
    public function setData($data=''){
        if(array_key_exists('key',$data)){
            $this->key="%".$data['key']."%";
        }
        if(array_key_exists('check',$data)){
            $this->check=$data['check'];
        }
        return $this;
    }

    public function collectdata($noofitem,$offset)
    {
        try {
            if($this->check=='all') {
                $query = "SELECT SQL_CALC_FOUND_ROWS * FROM users as a ,settings as b  WHERE (b.title LIKE :key OR b.fullname LIKE :key) AND (b.user_id=a.id) LIMIT $noofitem OFFSET $offset ";
            }
            else if($this->check=='title') {

                $query = "SELECT SQL_CALC_FOUND_ROWS * FROM users as a ,settings as b  WHERE (b.title LIKE :key ) AND (b.user_id=a.id) LIMIT $noofitem OFFSET $offset ";
            }
            else {

                $query = "SELECT SQL_CALC_FOUND_ROWS * FROM users as a ,settings as b  WHERE ( b.fullname LIKE :key) AND (b.user_id=a.id) LIMIT $noofitem OFFSET $offset ";

            }
                $stmt=$this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':key'=>$this->key
                )
            );

            $data = $stmt->fetchAll();

            $subquery = 'SELECT FOUND_ROWS()';
            $row = $this->pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);
            $data['totalRow'] = $row;
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}