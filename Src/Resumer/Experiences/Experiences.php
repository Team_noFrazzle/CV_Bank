<?php
namespace App\Resumer\Experiences;

use pdo;


class Experiences
{
    public $id = '';
    public $userid='';
    public $title='';
    public $date = '';
    public $Company = '';
    public $date3= '';
    public $error='';
    public $Board='';
    public $location='';
    public $unique='';
    public function setData($data = '',$userid='',$unique='')
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if ($unique) {
           $this->unique =$unique;
        }
        if (array_key_exists('Designation', $data)) {
            $this->title = $data['Designation'];
        }
        if (array_key_exists('date', $data)) {
            $this->date = $data['date'];
        }
        if (array_key_exists('Company', $data)) {
            $this->Company = $data['Company'];
        }
        if (array_key_exists('date3', $data)) {
            $this->date3 = $data['date3'];
        }
        if (array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }
        if ($userid) {
            $this->userid = $userid;
        }
        return $this;

    }
    public function __construct()
    {

        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }
    public function Store()
    {
        try {
            $query  ="INSERT INTO `experiences` (`id`, `user_id`, `designation`, `company_name`, `start_date`, `end_date`, `company_location`) VALUES (:id, :user_id, :designation, :companyname,:startdate, :enddate,:location)";


            $stmt =$this->pdo->prepare($query);

            $data = $stmt->execute(array(
              ':id'=>null,
                ':user_id'=>$this->userid,
                ':designation'=>$this->title,
                ':companyname'=>$this->Company,
                ':startdate'=>$this->date,
                ':enddate'=>$this->date3,
                ':location'=>$this->location

            ));

            if ($data) {
                $_SESSION['message']='Successfully ! Submitted';
                header("location:Create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function index()
    {

        try {

            $query  = "SELECT *FROM experiences WHERE  id=$this->unique";



            $stmt =$this->pdo->prepare($query);
             $stmt->execute();

         $data =$stmt->fetch();
        return $_SESSION['data']=$data;


        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function update()
    {


        try {


            $query  = "UPDATE `experiences` SET  `designation` = :c, `company_name` =:d, `start_date` =:e, `end_date` = :f, `company_location` = :g WHERE `experiences`.`id` =$this->id";

            $stmt = $this->pdo->prepare($query);


//            print_r($query);
//            die();
            $data =   $stmt->execute(array(

                ':c'=>$this->title,
                ':d'=>$this->Company,
                ':e'=>$this->date,
                ':f'=>$this->date3,
                ':g'=>$this->location
            ));

            if ($data) {
                $_SESSION['message']='Successfully ! Updated';
                header("location:EditForm.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function view()
    {


        try {

           // $query  = "SELECT p.* ,c.* FROM experiences as p , users as c WHERE c.id=p.user_id";

            $query=
                "SELECT  experiences.id,experiences.designation,experiences.company_name,experiences.start_date,experiences.end_date, experiences.company_location
FROM    experiences   
        LEFT JOIN users 
            ON experiences.user_id = users.id";

            $stmt =$this->pdo->prepare($query);
            $stmt->execute();

            $data =$stmt->fetchAll();

            return  $_SESSION['data']=$data;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Delete()
    {


        try {
            $query  = "Delete From experiences WHERE  id=$this->userid";


            $stmt =$this->pdo->prepare($query);
            $result=  $stmt->execute();

            if($result)
            {
                $_SESSION['message'] ='Successfully Deleted';
                header("location:index.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function PassingYear()
    {


        try {
            $query  = "Select *From year";


            $stmt =$this->pdo->prepare($query);
             $stmt->execute();
           return $result=$stmt->fetchAll();

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}
