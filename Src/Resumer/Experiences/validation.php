<?php
namespace App\Resumer\Experiences;

use App\Resumer\Experiences\Experiences;



class validation
{
    public $id = '';
    public $userid = '';
    public $title = '';
    public $date = '';
    public $Company = '';
    public $date3 = '';
    public $error = '';
    public $Board = '';
    public $location = '';

    public function setData($data = '', $userid = '')
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('Designation', $data)) {
            $this->title = $data['Designation'];
        }
        if (array_key_exists('date', $data)) {
            $this->date = $data['date'];
        }
        if (array_key_exists('Company', $data)) {
            $this->Company = $data['Company'];
        }
        if (array_key_exists('date3', $data)) {
            $this->date3 = $data['date3'];
        }
        if (array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }
        if ($userid) {
            $this->userid = $userid;
        }
        return $this;
    }

    public function validForInsert()
    {
//        echo "<pre>";
//        print_r($this);
//        die();
        $this->error = array();
        if (isset($_POST)) {

            if (empty($this->title)) {
                $this->error['Designation1'] = "<span style='color:red'>" . "Designation   required" . "</span>" . "</br>";
            }
            if (empty($this->Company)) {
                $this->error['company1'] = "<span style='color:red'>" . "company is required" . "</span>" . "</br>";
            }
            if (empty($this->date)) {
                $this->error['date1'] = "<span style='color:red'>" . "  Starting date  required" . "</span>" . "</br>";
            }
            if (empty($this->date3)) {
                $this->error['date2'] = "<span style='color:red'>" . "  End date  required" . "</span>" . "</br>";
            }
            if (empty($this->location)) {
                $this->error['Location1'] = "<span style='color:red'>" . " Duration can't is Empty " . "</span>" . "</br>";
            }
        }
        if (count($this->error) > 0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:Create.php');
        } else {
            $obj = new Experiences();

            $user_id = $_SESSION['user_info']['id'];

            $obj->setData($_POST, $user_id)->Store();

        }
    }

    public function updateValidation()


    {
//      echo "<pre>";
//        print_r($this);
//        die();
        $this->error = array();
        if (isset($_POST)) {

            if (empty($this->title)) {
                $this->error['Designation1'] = "<span style='color:red'>" . "Designation   required" . "</span>" . "</br>";
            }
            if (empty($this->Company)) {
                $this->error['company1'] = "<span style='color:red'>" . "company is required" . "</span>" . "</br>";
            }
            if (empty($this->date)) {
                $this->error['date1'] = "<span style='color:red'>" . "  Starting date  required" . "</span>" . "</br>";
            }
            if (empty($this->date3)) {
                $this->error['date2'] = "<span style='color:red'>" . "  End date  required" . "</span>" . "</br>";
            }
            if (empty($this->location)) {
                $this->error['Location1'] = "<span style='color:red'>" . " Duration can't is Empty " . "</span>" . "</br>";
            }
        }
        if (count($this->error) > 0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:EditForm.php');
        } else {
            $obj = new Experiences();

            $user_id = $_SESSION['user_info']['id'];

            $obj->setData($_POST, $user_id)->update();
        }
    }
}

