<?php
namespace App\Resumer\Posts;

use App\Resumer\Posts\Posts;



class validation
{
    public $id = '';
    public $userid = '';
    public $title = '';
    public $error = '';
    public $description='';
    public $tag='';
    public $categories='';
    public $uniqueid='';




    public function setData($data = '', $userid = '', $uniqueid='')
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('tag', $data)) {
            $this->tag = $data['tag'];
        }
        if (array_key_exists('categories', $data)) {
            $this->categories = $data['categories'];
        }
        if ($userid) {
            $this->userid = $userid;
        }
        if($uniqueid){
            $this->uniqueid=$uniqueid;
        }
        return $this;
    }

    public function validForInsert()
    {


        $this->error = array();
        if (isset($_POST)) {

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Title   required" . "</span>" . "</br>";
            }
            if (empty($this->description)) {
                $this->error['description1'] = "<span style='color:red'>" . "Description is required" . "</span>" . "</br>";
            }
            if (empty($this->tag)) {
                $this->error['tag1'] = "<span style='color:red'>" . "  Tag can't Empty  " . "</span>" . "</br>";
            }
            if (empty($this->categories)) {
                $this->error['categories1'] = "<span style='color:red'>" . "  Categories  required" . "</span>" . "</br>";
            }


        }

        if (count($this->error) > 0) {


            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:Create.php');
        } else {

            $obj = new Posts();

            $user_id = $_SESSION['user_info']['id'];

            $obj->setData($_POST, $user_id)->Store();

        }
    }

    public function updateValidation()


    {
//      echo "<pre>";
//        print_r($this);
//        die();

        $this->error = array();
        if (isset($_POST)) {

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Title   required" . "</span>" . "</br>";
            }
            if (empty($this->description)) {
                $this->error['description1'] = "<span style='color:red'>" . "Description is required" . "</span>" . "</br>";
            }
            if (empty($this->tag)) {
                $this->error['tag1'] = "<span style='color:red'>" . "  Tag can't Empty  " . "</span>" . "</br>";
            }
            if (empty($this->categories)) {
                $this->error['categories1'] = "<span style='color:red'>" . "  Categories  required" . "</span>" . "</br>";
            }
        }
        if (count($this->error) > 0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:EditForm.php');
        } else {
            $obj = new Posts();

            $user_id = $_SESSION['user_info']['id'];

            $obj->setData($_POST, $user_id)->update();
        }
    }
}

