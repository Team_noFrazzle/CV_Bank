<?php
namespace App\Resumer\Skills;

use pdo;


class Skills
{


    public $id='';
    public $userid='';
    public $title='';
    public $description= '';
    public $skilllev= '';
    public $experience= '';
    public $exparea= '';
    public $pdo='';
    public $tmp_nam='';


    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }
    public function setData($data = '',$userid='')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
            $this->title   = filter_var($this->title, FILTER_SANITIZE_STRING);
        }
        if ($userid) {
           $this->userid = $userid;
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];

            $this->description = filter_var($this->description, FILTER_SANITIZE_STRING);
        }

        if (array_key_exists('skilllev', $data)) {
            $this->skilllev = $data['skilllev'];

            $this->skilllev = filter_var($this->skilllev, FILTER_SANITIZE_STRING);
        }

        if (array_key_exists('experience', $data)) {
            $this->experience = $data['experience'];

            $this->experience = filter_var($this->experience, FILTER_SANITIZE_STRING);
        }

        if (array_key_exists('exparea', $data)) {
            $this->exparea = $data['exparea'];

            $this->exparea = filter_var($this->exparea, FILTER_SANITIZE_STRING);
        }



        return $this;



    }
    public function Store()
    {


        try {
            $query  = "INSERT INTO `skills` (`id`, `user_id`, `title`, `description`, `level`, `experience`, `experience_area`)
                         VALUES (:id, :user_id, :title, :des, :lev, :exp, :exparea)";
            $stmt =$this->pdo->prepare($query);
            $data = $stmt->execute([
              ':id'=>null,
                ':user_id'=>$this->userid,
                ':title'=>$this->title,
                ':des'=>$this->description,
                ':lev'=>$this->skilllev,
                ':exp'=>$this->exparea,
                ':exparea'=>$this->experience
            ]);
            if ($data) {
                $_SESSION['message']='Successfully ! Submitted';
                header("location:Create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function index()
    {

        try {

            $query  = "SELECT *FROM skills WHERE  user_id=$this->userid";

            $stmt =$this->pdo->prepare($query);
             $stmt->execute();

          $data =$stmt->fetch();

        return  $_SESSION['data']=$data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function update()
    {


        try {

            $query  = "UPDATE `skills` SET  `title` =:title, `description` =:des, `level` =:skilllev, `experience` =:exp, `experience_area` =:exparea  WHERE `skills`.`user_id` = $this->userid";

            $stmt = $this->pdo->prepare($query);
            $data =   $stmt->execute(array(

                ':title'=>$this->title,
                ':des'=>$this->description,
                ':skilllev'=>$this->skilllev,
                ':exp'=>$this->experience,
                ':exparea'=>$this->exparea
            ));

            if ($data) {
                $_SESSION['message']='Successfully ! Updated';
                header("location:EditForm.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function view()
    {


        try {

            $query  = "SELECT p.* ,c.* FROM skills as p , users as c WHERE p.user_id=c.id";

            $stmt =$this->pdo->prepare($query);
            $stmt->execute();

            $data =$stmt->fetchAll();

//            echo "<pre>";
//            print_r($data);
//            die();

            return  $_SESSION['data']=$data;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Delete()
    {


        try {
            $query  = "Delete From skills WHERE  user_id=$this->userid";


            $stmt =$this->pdo->prepare($query);
            $result=  $stmt->execute();

            if($result)
            {
                $_SESSION['message'] ='Successfully Deleted';
                header("location:index.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}
