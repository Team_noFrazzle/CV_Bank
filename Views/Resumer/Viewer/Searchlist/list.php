<?php
include "../../../../vendor/autoload.php";
use App\Resumer\Search\Search;
$obj=new Search;
if(!empty($_GET['page'])){
    $cpage=$_GET['page'];

}
else {
    $cpage = 0;
    $obj->setData($_POST);

}
$noofitem=2;
$offset=$noofitem*$cpage;
$data=$obj->collectdata($noofitem,$offset);
$totalrow = $data['totalRow']-1;
$totalpage=ceil($totalrow/$noofitem);
array_pop($data);



?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Material Design - Responsive Table</title>
  <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="../../../../assets/search_list_style/paginate.css">
  
      <link rel="stylesheet" href="../../../../assets/search_list_style/css/style.css">

  
</head>

<body>
  <div id="demo">
  <h1>Material Design Responsive Table</h1>
  <h2>Table of my other Material Design works (list was updated 08.2015)</h2>
  
  <!-- Responsive table starts here -->
  <!-- For correct display on small screens you must add 'data-title' to each 'td' in your table -->
  <div class="table-responsive-vertical shadow-z-1">
  <!-- Table starts here -->
  <table id="table" class="table table-hover table-mc-light-blue">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Link</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
      <?php
      $serial=0;
        foreach ($data as $value){
      ?>
        <tr>
          <td data-title="No"><?php echo ++$serial;?></td>
          <td data-title="Name"><?php echo $value['fullname']?></td>
            <td data-title="Title"><?php echo $value['title']?></td>
            <td data-title="Link">
            <a href="../../Viewer/profile/index.php" target="_blank">see profile</a>
          </td>
        </tr>
       <?php } ?>
      </tbody>
    </table>
  </div>
  
  <!-- Table Constructor change table classes, you don't need it in your project -->
     <?php if($offset>0){?>
      <div class="paginate paginate-dark wrapper">
          <ul>
              <li><a href="">&lang;</a></li>
              <?php for($i=0;$i<$totalpage;$i++){ ?>
              <li><a href="list.php?page=<?php echo $i;?>"><?php echo $i; ?></a></li>
              <?php }?>
              <li><a href="">&rang;</a></li>


          </ul>
      </div>
<?php  }?>
  <div style="width: 100%; ">

  </div>

</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="../../../../assets/search_list_style/js/index.js"></script>

</body>
</html>
