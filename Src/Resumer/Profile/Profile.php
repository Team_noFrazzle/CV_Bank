<?php
namespace App\Resumer\Profile;
use PDO;
class Profile
{
    public $id;

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }

    public function setData($data = '')
    {
        $this->id = $data['id'];


        return $this;
    }

    public function collect()
    {
        try {
            $query = "SELECT a.* ,b.* FROM users as a inner JOIN abouts as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['abouts'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN awards as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['awards'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN contacts as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['contacts'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN educations as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['educations'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN experiences as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['experiences'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN facts as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['facts'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN hobbies as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['hobbies'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN portfolios as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['portfolios'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN posts as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['posts'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN services as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['services'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN settings as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['settings'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        try {
            $query = "SELECT a.id ,b.* FROM users as a inner JOIN skills as b WHERE (a.id=b.user_id)  ";
            echo $this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id
                )
            );
            $db['skills'] = $stmt->fetch();

        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        return $db;
    }
}
