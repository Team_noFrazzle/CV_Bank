<?php
namespace App\Resumer\Educations;

use App\Resumer\Educations\Educations;



class validation
{
    public $id = '';
    public $userid='';
    public $title='';
    public $result = '';
    public $institute = '';
    public $majorsub = '';
    public $year = '';
    public $error='';
    public $Board='';
    public $duraion='';
    public function setData($data = '',$userid='')
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('result', $data)) {
            $this->result = $data['result'];
        }
        if (array_key_exists('ins', $data)) {
            $this->institute = $data['ins'];
        }
        if (array_key_exists('msub', $data)) {
            $this->majorsub = $data['msub'];
        }
        if (array_key_exists('year', $data)) {
            $this->year = $data['year'];
        }
        if (array_key_exists('Board', $data)) {
            $this->Board = $data['Board'];
        }
        if (array_key_exists('Duration', $data)) {
            $this->duraion = $data['Duration'];
        }
        if ($userid) {
            $this->userid = $userid;
        }
        return $this;
    }
    public function validForInsert()
    {
//        echo "<pre>";
//        print_r($this);
//        die();
        $this->error = array();
        if (isset($_POST)) {

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Subject Name   required" . "</span>" . "</br>";
            }
            if (empty($this->result)) {
                $this->error['result1'] = "<span style='color:red'>" . "Result is required" . "</span>" . "</br>";
            }
            if (empty($this->institute)) {
                $this->error['ins1'] = "<span style='color:red'>" . "Institution  Name is required" . "</span>" . "</br>";
            }
            if (empty($this->majorsub)) {
                $this->error['msub1'] = "<span style='color:red'>" . "  Main subject Name Required" . "</span>" . "</br>";
            }
            if ($this->year=="1") {
                $this->error['year1'] = "<span style='color:red'>" . "Year can't Empty" . "</span>" . "</br>";
            }
            if (empty($this->duraion)) {
                $this->error['Duration1'] = "<span style='color:red'>" . " Duration can't is Empty " . "</span>" . "</br>";
            }
            if (empty($this->Board)) {
                $this->error['Board1'] = "<span style='color:red'>" . " Board can't is Empty " . "</span>" . "</br>";
            }



        }
        if (count($this->error) >0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:Create.php');
        } else {
            $obj= new Educations();

            $user_id= $_SESSION['user_info']['id'];

            $obj->setData($_POST,$user_id)->Store();

        }
    }

    public function updateValidation()


        {
//        echo "<pre>";
//        print_r($this);
//        die();
            $this->error = array();
            if (isset($_POST)) {

                if (empty($this->title)) {
                    $this->error['title1'] = "<span style='color:red'>" . "Subject Name   required" . "</span>" . "</br>";
                }
                if (empty($this->result)) {
                    $this->error['result1'] = "<span style='color:red'>" . "Result is required" . "</span>" . "</br>";
                }
                if (empty($this->institute)) {
                    $this->error['ins1'] = "<span style='color:red'>" . "Institution  Name is required" . "</span>" . "</br>";
                }
                if (empty($this->majorsub)) {
                    $this->error['msub1'] = "<span style='color:red'>" . "  Main subject Name Required" . "</span>" . "</br>";
                }
                if ($this->year == "1") {
                    $this->error['year1'] = "<span style='color:red'>" . "Year can't Empty" . "</span>" . "</br>";
                }


            }
            if (count($this->error) > 0) {
                session_start();
                $_SESSION['fail'] = $this->error;

                header('location:EditForm.php');
            } else {
                $obj = new Educations();

                $user_id = $_SESSION['user_info']['id'];

                $obj->setData($_POST, $user_id)->update();

            }
        }
    }

