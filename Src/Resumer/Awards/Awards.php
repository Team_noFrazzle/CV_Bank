<?php
namespace App\Resumer\Awards;

use pdo;


class Awards
{
    public $id = '';
    public $userid = '';
    public $title = '';
    public $organization = '';
    public $error = '';
    public $Description = '';
    public $location = '';
    public $year = '';
    public $uniqueid = '';

    public function setData($data = '', $userid = '', $uniqueid = '')
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('Description', $data)) {
            $this->Description = $data['Description'];
        }

        if (array_key_exists('organization', $data)) {
            $this->organization = $data['organization'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }
        if (array_key_exists('year', $data)) {
            $this->year = $data['year'];
        }
        if ($userid) {
            $this->userid = $userid;
        }
        if ($uniqueid) {
            $this->uniqueid = $uniqueid;
        }
        return $this;

    }

    public function __construct()
    {

        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }

    public function Store()
    {
        try {
            $query = "INSERT INTO `awards` (`id`, `user_id`, `title`, `organization`, `description`, `location`, `year`) VALUES (:a,:b,:p,:d, :e,:f,:g)";
            $stmt = $this->pdo->prepare($query);

            $data = $stmt->execute(array(
                ':a' => null,
                ':b' => $this->userid,
                ':p' => $this->title,
                ':d' => $this->organization,
                ':e' => $this->Description,
                ':f' => $this->location,
                ':g' => $this->year

            ));
            if ($data) {
                $_SESSION['message'] = 'Successfully ! Submitted';
                header("location:Create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function index()
    {

        try {

            $query = "SELECT *FROM awards WHERE  id=$this->uniqueid";


//            print_r($query);
//            die();

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            $data = $stmt->fetch();
            return $_SESSION['data'] = $data;


        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function update()
    {


        try {


            $query = "UPDATE `awards` SET   `title` =:p, `organization`=:d, `description` =:e, `location` =:f, `year` = :g WHERE `awards`.`id` =$this->id";

            $stmt = $this->pdo->prepare($query);


            $data = $stmt->execute(array(


                ':p' => $this->title,
                ':d' => $this->organization,
                ':e' => $this->Description,
                ':f' => $this->location,
                ':g' => $this->year
            ));

            if ($data) {
                $_SESSION['message'] = 'Successfully ! Updated';
                header("location:EditForm.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function view()
    {


        try {

            // $query  = "SELECT p.* ,c.* FROM experiences as p , users as c WHERE c.id=p.user_id";

            $query =
                "SELECT  awards.id,awards.title,awards.organization,awards.description,awards.location, awards.year
FROM    awards   
        LEFT JOIN users 
            ON awards.user_id = users.id";

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            $data = $stmt->fetchAll();

            return $_SESSION['data'] = $data;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Delete()
    {


        try {
            $query = "Delete From Awards WHERE  id=$this->uniqueid";


            $stmt = $this->pdo->prepare($query);
            $result = $stmt->execute();

            if ($result) {
                $_SESSION['message'] = 'Successfully Deleted';
                header("location:index.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function PassingYear()
    {


        try {
            $query = "Select *From year";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            return $result = $stmt->fetchAll();

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}
