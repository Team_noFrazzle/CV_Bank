<?php
namespace App\Resumer\Hobbies;

use pdo;


class Hobbies
{


    public $id='';
    public $userid='';
    public $title='';
    public $img='';
    public $description= '';
    public $pdo='';
    public $tmp_nam='';


    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }
    public function setData($data = '',$userid='')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
            $this->title   = filter_var($this->title, FILTER_SANITIZE_STRING);
        }
        if ($userid) {
           $this->userid = $userid;

        }
        if ($_FILES) {


           $this->tmp_nam=$_FILES['img']['tmp_name'];
            $this->img = $_FILES['img']['name'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];

            $this->description = filter_var($this->description, FILTER_SANITIZE_STRING);
        }

        return $this;



    }
    public function Store()
    {


        try {
            move_uploaded_file($this->tmp_nam,'../../../../image/'.$this->img );

            $query  = "INSERT INTO `hobbies` (`id`, `user_id`, `title`, `description`, `img`)
                         VALUES (:id, :user_id, :title, :des, :img)";
            $stmt =$this->pdo->prepare($query);
            $data = $stmt->execute(array(
              ':id'=>null,
                ':user_id'=>$this->userid,
                ':title'=>$this->title,
                ':des'=>$this->description,
                ':img'=>$this->img
            ));
            if ($data) {
                $_SESSION['message']='Successfully ! Submitted';
                header("location:Create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function index()
    {

        try {

            $query  = "SELECT *FROM hobbies WHERE  user_id=$this->userid";

            $stmt =$this->pdo->prepare($query);
             $stmt->execute();

          $data =$stmt->fetch();

        return  $_SESSION['data']=$data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function update()
    {


        try {

            move_uploaded_file($this->tmp_nam,'../../../../image/'.$this->img );
            $query  = "UPDATE `hobbies` SET  `title` =:title, `description` =:des, `img` =:img  WHERE `hobbies`.`user_id` = $this->userid";

            $stmt = $this->pdo->prepare($query);
            $data =   $stmt->execute(array(

                ':title'=>$this->title,
                ':des'=>$this->description,
                ':img'=>$_POST['img']
            ));

            if ($data) {
                $_SESSION['message']='Successfully ! Updated';
                header("location:EditForm.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function view()
    {


        try {

            $query  = "SELECT p.* ,c.* FROM hobbies as p , users as c WHERE p.user_id=c.id";

            $stmt =$this->pdo->prepare($query);
            $stmt->execute();

            $data =$stmt->fetchAll();

            return  $_SESSION['data']=$data;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Delete()
    {


        try {
            $query  = "Delete From hobbies WHERE  user_id=$this->userid";


            $stmt =$this->pdo->prepare($query);
            $result=  $stmt->execute();

            if($result)
            {
                $_SESSION['message'] ='Successfully Deleted';
                header("location:index.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}
