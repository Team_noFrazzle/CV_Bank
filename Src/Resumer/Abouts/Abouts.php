<?php
namespace App\Resumer\Abouts;

use pdo;

class Abouts
{
    public $id='';
    public $userid='';
    public $title='';
    public $phone='';
    public $bio = '';
    public $pdo='';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }
    public function setData($data = '',$userid='')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
            $this->title   = filter_var($this->title, FILTER_SANITIZE_STRING);
        }
        if ($userid) {
            $this->userid = $userid;

        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
            $this->phone  =filter_var($this->phone, FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('bio', $data)) {
            $this->bio = $data['bio'];

            $this->bio  =filter_var($this->bio, FILTER_SANITIZE_STRING);

        }
        return $this;
    }
    public function Store()
    {


        try {
            $query  = "INSERT INTO `abouts` (`id`, `user_id`, `title`, `phone`, `bio`) VALUES (:id, :user_id, :title, :phone, :bio)";

            $stmt =$this->pdo->prepare($query);
            $data = $stmt->execute(array(
              ':id'=>null,
                ':user_id'=>$this->userid,
                ':title'=>$this->title,
                ':phone'=>$this->phone,
                ':bio'=>$this->bio
            ));
            if ($data) {
                $_SESSION['message']='Successfully ! Submitted';
                header("location:Create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function index()
    {


        try {
            $query  = "SELECT *FROM abouts WHERE  user_id=$this->userid";

            $stmt =$this->pdo->prepare($query);
             $stmt->execute();

          $data =$stmt->fetch();

        return  $_SESSION['data']=$data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function update()
    {


        try {
            $query  = "UPDATE `abouts` SET  `title` =:title, `phone` =:phone, `bio` =:bio WHERE `abouts`.`user_id` = $this->userid";

            $stmt = $this->pdo->prepare($query);
            $data =   $stmt->execute(array(

                ':title'=>$this->title,
                ':phone'=>$this->phone,
                ':bio'=>$this->bio
            ));

            if ($data) {
                $_SESSION['message']='Successfully ! Updated';
                header("location:EditForm.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function view()
    {


        try {
            $query  = "SELECT p.* ,c.* FROM abouts as p , users as c WHERE p.user_id=c.id";

            $stmt =$this->pdo->prepare($query);
            $stmt->execute();

            $data =$stmt->fetchAll();

            return  $_SESSION['data']=$data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function Delete()
    {


        try {
            $query  = "Delete From abouts WHERE  user_id=$this->userid";


            $stmt =$this->pdo->prepare($query);
            $result=  $stmt->execute();

            if($result)
            {
                $_SESSION['message'] ='Successfully Deleted';
                header("location:index.php");
            }
            else {
                echo "not";
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}
