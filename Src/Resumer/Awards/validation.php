<?php
namespace App\Resumer\Awards;

use App\Resumer\Awards\Awards;



class validation
{
    public $id = '';
    public $userid = '';
    public $title = '';
    public $organization='';
    public $error = '';
    public $Description = '';
    public $location = '';
    public $year='';

    public function setData($data = '', $userid = '')
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('Description', $data)) {
            $this->Description = $data['Description'];
        }

        if (array_key_exists('organization', $data)) {
            $this->organization = $data['organization'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }if (array_key_exists('year', $data)) {
            $this->year = $data['year'];
        }
        if ($userid) {
            $this->userid = $userid;
        }
        return $this;
    }

    public function validForInsert()
    {
//       echo "<pre>";
//      print_r($this);
//      die();
        $this->error = array();
        if (isset($_POST)) {

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Title   required" . "</span>" . "</br>";
            }
            if (empty($this->Description)) {
                $this->error['description1'] = "<span style='color:red'>" . "Description is required" . "</span>" . "</br>";
            }
            if (empty($this->organization)) {
                $this->error['organization1'] = "<span style='color:red'>" . "Organization is  required" . "</span>" . "</br>";
            }
            if ($this->year=='1') {
                $this->error['year1'] = "<span style='color:red'>" . "  Year is  required" . "</span>" . "</br>";
            }
            if (empty($this->location)) {
                $this->error['Location1'] = "<span style='color:red'>" . " Location can't is Empty " . "</span>" . "</br>";
            }
        }
        if (count($this->error) > 0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:Create.php');
        } else {
            $obj = new Awards();

            $user_id = $_SESSION['user_info']['id'];

            $obj->setData($_POST, $user_id)->Store();

        }
    }

    public function updateValidation()

    {
    //       echo "<pre>";
//      print_r($this);
//      die();
        $this->error = array();
        if (isset($_POST)) {

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Title   required" . "</span>" . "</br>";
            }
if (empty($this->Description)) {
    $this->error['description1'] = "<span style='color:red'>" . "Description is required" . "</span>" . "</br>";
}
if (empty($this->organization)) {
    $this->error['organization1'] = "<span style='color:red'>" . "Organization is  required" . "</span>" . "</br>";
}
if ($this->year=='1') {
    $this->error['year1'] = "<span style='color:red'>" . "  Year is  required" . "</span>" . "</br>";
}
if (empty($this->location)) {
    $this->error['Location1'] = "<span style='color:red'>" . " Location can't is Empty " . "</span>" . "</br>";
}
}
if (count($this->error) > 0) {
    session_start();
    $_SESSION['fail'] = $this->error;

    header('location:EditForm.php');
} else {
    $obj = new Awards();

    $user_id = $_SESSION['user_info']['id'];

    $obj->setData($_POST, $user_id)->update();

}
}


}

